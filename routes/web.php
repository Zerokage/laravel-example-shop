<?php

use App\Http\Controllers\Category\CreateController as CategoryCreateController;
use App\Http\Controllers\Category\IndexController as CategoryIndexController;
use App\Http\Controllers\Category\StoreController as CategoryStoreController;
use App\Http\Controllers\Category\EditController as CategoryEditController;
use App\Http\Controllers\Category\ShowController as CategoryShowController;
use App\Http\Controllers\Category\UpdateController as CategoryUpdateController;
use App\Http\Controllers\Category\DeleteController as CategoryDeleteController;

use App\Http\Controllers\User\CreateController as UserCreateController;
use App\Http\Controllers\User\IndexController as UserIndexController;
use App\Http\Controllers\User\StoreController as UserStoreController;
use App\Http\Controllers\User\EditController as UserEditController;
use App\Http\Controllers\User\ShowController as UserShowController;
use App\Http\Controllers\User\UpdateController as UserUpdateController;
use App\Http\Controllers\User\DeleteController as UserDeleteController;

use App\Http\Controllers\Product\CreateController as ProductCreateController;
use App\Http\Controllers\Product\IndexController as ProductIndexController;
use App\Http\Controllers\Product\StoreController as ProductStoreController;
use App\Http\Controllers\Product\EditController as ProductEditController;
use App\Http\Controllers\Product\ShowController as ProductShowController;
use App\Http\Controllers\Product\UpdateController as ProductUpdateController;
use App\Http\Controllers\Product\DeleteController as ProductDeleteController;

use App\Http\Controllers\Main\IndexController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'admin'], function () {
    #Route::get('/', IndexController::class)->name('main.index');
    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', CategoryIndexController::class)->name('category.index');
        Route::get('/create', CategoryCreateController::class)->name('category.create');
        Route::post('/', CategoryStoreController::class)->name('category.store');
        Route::get('/{category}/edit', CategoryEditController::class)->name('category.edit');
        Route::get('/{category}', CategoryShowController::class)->name('category.show');
        Route::patch('/{category}', CategoryUpdateController::class)->name('category.update');
        Route::delete('/{category}', CategoryDeleteController::class)->name('category.delete');
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('/', UserIndexController::class)->name('user.index');
        Route::get('/create', UserCreateController::class)->name('user.create');
        Route::post('/', UserStoreController::class)->name('user.store');
        Route::get('/{user}/edit', UserEditController::class)->name('user.edit');
        Route::get('/{user}', UserShowController::class)->name('user.show');
        Route::patch('/{user}', UserUpdateController::class)->name('user.update');
        Route::delete('/{user}', UserDeleteController::class)->name('user.delete');
    });

    Route::group(['prefix' => 'products'], function () {
        Route::match(['get', 'post'],'/', ProductIndexController::class)->name('product.index');
        Route::get('/create', ProductCreateController::class)->name('product.create');
        Route::post('/store', ProductStoreController::class)->name('product.store');
        Route::get('/{product}/edit', ProductEditController::class)->name('product.edit');
        Route::get('/{product}', ProductShowController::class)->name('product.show');
        Route::patch('/{product}', ProductUpdateController::class)->name('product.update');
        Route::delete('/{product}', ProductDeleteController::class)->name('product.delete');
    });

    Route::group(['prefix' => 'orders'], function () {
        Route::get('/', App\Http\Controllers\Order\IndexController::class)->name('order.index');
        Route::get('/{order}', App\Http\Controllers\Order\ShowController::class)->name('order.show');
        Route::get('/{order}/edit', App\Http\Controllers\Order\EditController::class)->name('order.edit');
        Route::patch('/{order}', App\Http\Controllers\Order\UpdateController::class)->name('order.update');
        Route::delete('/{order}', App\Http\Controllers\Order\DeleteController::class)->name('order.delete');
    });
});
