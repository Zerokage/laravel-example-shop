<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'orders'], function () {
    Route::get('/', \App\Http\Controllers\API\Order\IndexController::class);
    Route::post('/', \App\Http\Controllers\API\Order\StoreController::class);
    Route::get('/{order}', \App\Http\Controllers\API\Order\ShowController::class);
    Route::patch('/{order}', \App\Http\Controllers\API\Order\UpdateController::class);
    Route::delete('/{order}', \App\Http\Controllers\API\Order\DeleteController::class);
});
Route::group(['prefix' => 'categories'], function () {
    Route::get('/', \App\Http\Controllers\API\Category\IndexController::class);
    Route::post('/', \App\Http\Controllers\API\Category\StoreController::class);
    Route::get('/{category}', \App\Http\Controllers\API\Category\ShowController::class);
    Route::patch('/{category}', \App\Http\Controllers\API\Category\UpdateController::class);
    Route::delete('/{category}', \App\Http\Controllers\API\Category\DeleteController::class);
});
Route::group(['prefix' => 'products'], function () {
    Route::post('/', \App\Http\Controllers\API\Product\IndexController::class);
    Route::get('/filters', \App\Http\Controllers\API\Product\FilterController::class);
    Route::post('/store', \App\Http\Controllers\API\Product\StoreController::class);
    Route::get('/filters', \App\Http\Controllers\API\Product\FilterController::class);
    Route::get('/{product}', \App\Http\Controllers\API\Product\ShowController::class);
    Route::patch('/{product}', \App\Http\Controllers\API\Product\UpdateController::class);
    Route::delete('/{product}', \App\Http\Controllers\API\Product\DeleteController::class);
    
});