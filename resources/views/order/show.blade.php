@extends('layouts.main')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Заказ</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">Главная</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex p-3">
                            <div class="mr-3">
                                <a href="{{ route('order.edit', $order->id) }}" class="btn btn-primary">Редактировать</a>
                            </div>
                            <form action="{{ route('order.delete', $order->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <input type="submit" class="btn btn-danger" value="Удалить">
                            </form>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <tr>
                                    <td>ID</td>
                                    <td>{{ $order->id }}</td>
                                </tr>
                                <tr>
                                    <td>Пользователь</td>
                                    <td>{{ $order->user->name }}</td>
                                </tr>
                                <tr>
                                <tr>
                                    <td>Email</td>
                                    <td>{{ $order->user->email }}</td>
                                </tr>
                                <tr>
                                    <td>Адресс</td>
                                    <td>{{ $order->user->address }}</td>
                                </tr>
                                <tr>
                                    <td>Итоговая цена</td>
                                    <td>{{ $order->total_price }}</td>
                                </tr>
                                <tr>
                                    <td>Статус оплаты</td>
                                    <td>{{ $statuses[$order->payment_status] }}</td>
                                </tr>
                                <tr>
                                    <td>Товары</td>
                                    <td>
                                        <table class="table table-hover text-nowrap">
                                            <tr>
                                                <td>ID</td>
                                                <td>Название</td>
                                                <td>Картинка</td>
                                                <td>Количество</td>
                                                <td>Цена за шт</td>
                                            </tr>
                                            @foreach ($order->products as $item)
                                                <tr>
                                                    <td>{{ $item->id }}</td>
                                                    <td>{{ $item->title }}</td>
                                                    <td>
                                                        @if ($item->id > 200)
                                                            <img width="100" height="100"
                                                                src="{{ Storage::disk('local')->url($item->preview_image) }}" />
                                                        @else
                                                            <img width="100" height="100"
                                                                src="{{ $item->preview_image }}" />
                                                        @endif
                                                    </td>
                                                    <td>{{ $item->qty }}</td>
                                                    <td>{{ $item->price }}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@endsection
