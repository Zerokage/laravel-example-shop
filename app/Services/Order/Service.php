<?php

namespace App\Services\Order;

use App\Models\Order;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class Service
{
    public function store($data)
    {
        try {
            DB::beginTransaction();
            $password = Hash::make(12345);

            $user = User::firstOrCreate([
                'email' => $data['email']
            ], [
                'name' => $data['name'],
                'address' => $data['address'],
                'password' => $password
            ]);

            $order = Order::create([
                'products' => json_encode($data['products']),
                'user_id' => $user->id,
                'total_price' => $data['total_price']
            ]);
            DB::commit();
        } catch (\Exception $exeption) {
            DB::rollBack();
            return $exeption -> getMessage();
        }
        return $order;
    }

    public function update(Order $order, $data)
    {
        try {
            DB::beginTransaction();
            $user = User::find($order->user_id);
            $user->update([
                'name' => $data['name'],
                'address' => $data['address'],
                'email' => $data['email']
            ]);
            $order->update([
                'user_id' => $user->id,
                'total_price' => $data['total_price'],
                'payment_status' => $data['payment_status']
            ]);
            DB::commit();
        } catch (\Exception $exeption) {
            DB::rollBack();
            return $exeption -> getMessage();
        }

        return $order->fresh();
    }
}
