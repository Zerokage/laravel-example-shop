<?php

namespace App\Services\Product;

use App\Models\Product;
use Illuminate\Support\Facades\Storage;

class Service
{
    public function store($data)
    {
        $data['preview_image'] = Storage::disk('public')->put('/images', $data['preview_image']);
        $product = Product::firstOrCreate([
            'title' => $data['title']
        ], $data);
        return $product;
    }

    public function update(Product $product, $data)
    {
        if (isset($data['preview_image'])) {
            $this->deleteFile($product->preview_image);
            $data['preview_image'] = Storage::disk('public')->put('/images', $data['preview_image']);
        }
        $product->update($data);
        
        return $product->fresh();
    }

    public function delete(Product $product)
    {
        $this->deleteFile($product->preview_image);
        $product->delete();
    }

    public function deleteFile($filePath)
    {
        if(Storage::disk('local')->exists('public/'.$filePath)){
            Storage::disk('local')->delete('public/'.$filePath);
        }
    }
}
