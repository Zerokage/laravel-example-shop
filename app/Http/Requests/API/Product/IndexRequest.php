<?php

namespace App\Http\Requests\API\Product;

use App\Http\Requests\API\BaseRequest;

class IndexRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'categories' => 'nullable|array',
            'prices' => 'nullable|array',
            'page' => 'required|integer'
        ];
    }
}
