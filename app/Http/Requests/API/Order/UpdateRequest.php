<?php

namespace App\Http\Requests\API\Order;

use App\Http\Requests\API\BaseRequest;

class UpdateRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'products' => 'required|array',
            'name' => 'required|string',
            'email' => 'required|string',
            'address' => 'required|string',
            'total_price' => 'required|integer'
        ];
    }
}
