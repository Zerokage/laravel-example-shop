<?php

namespace App\Http\Controllers\API\Product;

use App\Http\Controllers\Controller;
use App\Http\Resources\Product\ProductResource;
use App\Models\Category;
use App\Models\Product;

class FilterController extends Controller
{
    public function __invoke()
    {
        $categories = Category::all();
        $maxPrice = Product::orderBy('price', 'DESC')->first()->price;
        $minPrice = Product::orderBy('price', 'ASC')->first()->price;
        $result = [
            'categories' => $categories,
            'price' => [
                'min' => $minPrice,
                'max' => $maxPrice
            ]
        ];
        return response()->json($result);
    }
}
