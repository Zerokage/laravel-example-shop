<?php

namespace App\Http\Controllers\API\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Product\UpdateRequest;
use App\Http\Resources\Product\ProductResource;
use App\Models\Product;
use Illuminate\Support\Facades\Storage;

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request, Product $product)
    {
        $data = $request->validated();

        if (isset($data['preview_image'])) {
            if(Storage::exists($product->preview_image)){
                Storage::delete($product->preview_image);
            }
            $data['preview_image'] = Storage::disk('public')->put('/images', $data['preview_image']);
        }
        $product->update($data);

        return new ProductResource($product->fresh());
    }
}
