<?php

namespace App\Http\Controllers\API\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Product\StoreRequest;
use App\Http\Resources\Product\ProductResource;
use App\Models\Product;
use Illuminate\Support\Facades\Storage;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request->validated();
    
        $data['preview_image'] = Storage::disk('public')->put('/images', $data['preview_image']);
        
        $product = Product::firstOrCreate([
            'title' => $data['title']
        ], $data);

        return new ProductResource($product);
    }
}
