<?php

namespace App\Http\Controllers\API\Order;

use App\Http\Requests\API\Order\UpdateRequest;
use App\Http\Resources\Order\OrderResource;
use App\Models\Order;

class UpdateController extends BaseController
{
    public function __invoke(UpdateRequest $request, Order $order)
    {
        $data = $request->validated();

        $order = $this->service->update($order, $data);
        
        return $order instanceof Order ? new OrderResource($order) : $order;    
    }
}
