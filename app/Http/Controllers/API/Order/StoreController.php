<?php

namespace App\Http\Controllers\API\Order;

use App\Http\Requests\API\Order\StoreRequest;
use App\Http\Resources\Order\OrderResource;
use App\Models\Order;

class StoreController extends BaseController
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request->validated();
        $order = $this->service->store($data);
        
        return $order instanceof Order ? new OrderResource($order) : $order;
    }
}
