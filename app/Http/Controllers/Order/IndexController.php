<?php

namespace App\Http\Controllers\Order;

use App\Models\Order;

class IndexController extends BaseController
{
    public function __invoke()
    {
        $orders = Order::paginate(5);
        $statuses = Order::payStatus();
        return view('order.index', compact('orders', 'statuses'));
    }
}
