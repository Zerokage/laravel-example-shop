<?php

namespace App\Http\Controllers\Order;

use App\Http\Requests\Order\UpdateRequest;
use App\Models\Order;
use App\Models\User;

class UpdateController extends BaseController
{
    public function __invoke(UpdateRequest $request, Order $order)
    {
        $data = $request->validated();
        
        $answer = $this->service->update($order, $data);
        
        $status = 'error';
        $message = 'Save error.';
        if ( $answer instanceof Order ) {
            $status = 'success';
            $message = 'Update successfully.';
            $order = $answer;        
        }
        return redirect()->route('order.show', $order)->with($status, $message);
    }
}
