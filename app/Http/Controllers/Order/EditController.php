<?php

namespace App\Http\Controllers\Order;

use App\Models\Order;

class EditController extends BaseController
{
    public function __invoke(Order $order)
    {
        $statuses = Order::payStatus();
        return view('order.edit', compact('order', 'statuses'));
    }
}
