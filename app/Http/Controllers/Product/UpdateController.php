<?php

namespace App\Http\Controllers\Product;

use App\Http\Requests\API\Product\UpdateRequest as ProductUpdateRequest;
use App\Models\Product;

class UpdateController extends BaseController
{
    public function __invoke(ProductUpdateRequest $request, Product $product)
    {
        $data = $request->validated();
        
        $product = $this->service->update($product, $data);

        return view('product.show', compact('product'));
    }
}
