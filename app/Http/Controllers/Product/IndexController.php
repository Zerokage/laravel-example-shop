<?php

namespace App\Http\Controllers\Product;

use App\Http\Filters\ProductFilter;
use App\Http\Requests\Product\IndexRequest as ProductIndexRequest;
use App\Models\Product;

class IndexController extends BaseController
{
    public function __invoke(ProductIndexRequest $request)
    {
        $data = $request->validated();
        $filter = app()->make(ProductFilter::class, ['queryParams' => array_filter($data)]);
        $products = Product::filter($filter)->paginate(5);
        return view('product.index', compact('products'));
    }
}
