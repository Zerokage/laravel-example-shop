<?php

namespace App\Http\Controllers\Product;

use App\Http\Requests\API\Product\StoreRequest as ProductStoreRequest;

class StoreController extends BaseController
{
    public function __invoke(ProductStoreRequest $request)
    {
        $data = $request->validated();
        
        $this->service->store($data);
        
        return redirect()->route('product.index');
    }
}
