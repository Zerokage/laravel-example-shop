<?php

namespace App\Models;

use App\Models\Traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory, Filterable;
    
    protected $table = 'products';
    protected $guarded = false;

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function getImageLinkAttribute()
    {
        return !strpos('http', $this->attributes['preview_image']) ? $this->attributes['preview_image'] : url('storage/' . $this->attributes['preview_image']);
    }
}
