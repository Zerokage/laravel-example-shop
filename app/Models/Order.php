<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'orders';
    protected $guarded = false;

    const PAYMENT_NEW = 1;
    const PAYMENT_PAYD = 2;

    public static function payStatus()
    {
        return [
            self::PAYMENT_NEW =>'Новый',
            self::PAYMENT_PAYD    => 'Оплачен'
        ];
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getProductsAttribute($value)
    {
        return json_decode($this->attributes['products']);
    }
}
