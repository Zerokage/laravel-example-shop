<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class OrderFactory extends Factory
{
    protected $model = Order::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $products = [];
        $totalPrice = 0;
        $n = rand(1, 3);
        for($i = 0; $i < $n; $i++){
            $product = Product::get()->random();
            $newProduct = [
                'id' => $product->id,
                'preview_image' => $product->preview_image,
                'price' => $product->price,
                'qty' => rand(1, 3),
                'title' => $product->title
            ];
            $totalPrice += $newProduct['qty'] * $newProduct['price']; 
            $products[] = $newProduct;
        }
        return [
            'payment_status' => 1,
            'user_id' => User::get()->random()->id,
            'products' => json_encode($products),
            'total_price' => $totalPrice,
        ];
    }
}
